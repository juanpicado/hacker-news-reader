'use strict';

define(['underscore', 'backbone',
  'backbone.marionette',
  'app/application',
  'collections/NewsCollection',
  'collections/CommentsCollections',
  'views/NewsItemDetailView',
  'views/NewsView'], function(
        _,
        Backbone,
        Marionette,
        NewsApp,
        NewsCollection,
        CommentsCollection,
        NewsItemDetailView,
        NewsView) {

    var Controller = Marionette.Controller.extend({
        initialize : function(options) {
            _.bindAll(this, "listNews", "onClose");
            this.newsCollection = new NewsCollection();
            this.commentsCollection = new CommentsCollection();
            this.NewsCollectionView = new NewsView({
                collection : this.newsCollection
              });
          },

        onClose: function() {
            //  put custom code here, to close this controller
            delete this.newsCollection;
            delete this.NewsCollectionView;
          },

        /**
         * Initialized on start, without hash
         * @method
         */
        listNews : function () {
            var that = this;
            // event to destro the preview.
            Backbone.on("preview:destroy", function () {

            }, this);

            // event triggered afer item added in the news collection
            this.NewsCollectionView.on("before:item:added", function(viewInstance){
                //console.log("sfore:item:addedt", viewInstance);
            });

            // event triggered for each item inside the collection
            this.listenTo(this.NewsCollectionView, "itemview:item:load-comments", function(options) {
                var discuss = 'https://news.ycombinator.com/item?id=',
                id = options.options.model.get('item_id'),
                // define if the news has comments
                hasComments = options.options.model.get('comments') === "discuss" ? false : true;
                if (hasComments) {
                  // if the news has comments, we check if the commentas was previously loaded
                  if (options.loaded) {
                    // only display the hidden comments
                    options.displayComments();
                  } else {
                    // if not exist previous comments loaded, fetch from the server
                    that.commentsCollection.itemId = id;
                    //
                    var TreeComment = Backbone.Model.extend({
                        initialize: function(){
                            var nodes = this.get("children");
                            if (nodes) {
                              this.childrens = new TreeCommentCollection(nodes);
                              this.unset("children");
                            }
                          }
                      });

                    var TreeCommentCollection = Backbone.Collection.extend({
                        model: TreeComment
                      });

                    that.commentsCollection.fetch({
                        success: function(response, xhr) {
                          var tree = new TreeCommentCollection(response.toJSON());
                          var treeView = new NewsItemDetailView({
                              collection: tree
                            });
                          treeView.render();
                          // append comments and
                          options.appendComments(treeView);
                          //
                          options.displayComments();
                          //
                          options.loaded = true;
                        },
                        error: function (errorResponse) {
                          console.error('There is an error with the connection', errorResponse);
                        }
                      });
                  }
                } else {
                  window.open(discuss + id);
                }
              });
            // triggered
            this.listenTo(this.NewsCollectionView, "itemview:item:load-preview", function(options) {
                var u = 'https://news.ycombinator.com/',
                model_url = options.options.model.get('url');
                if (model_url.indexOf('item') !== 0) {
                  $("#frame").attr("src", model_url);
                  options.displayPreview();
                } else {
                  //TODO: message
                }

              });
            NewsApp.reader.show(this.NewsCollectionView);

            // fetch news form hacker news api
            var refresh = function() {
              that.newsCollection.fetch({
                success: function(response, xhr) {},
                error: function (errorResponse) {
                    console.error('There is an error with the connection', errorResponse);
                  }
              });
            };

            // fech after load
            refresh();

            // event to refresh news from another part of the app
            Backbone.on("news:refresh", function () {
                refresh();
              }, this);
          }
      });

    return Controller;
  }
);
