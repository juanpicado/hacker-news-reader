'use strict';

window.key = '60ee3a11c1ca4365889570e47b14bb37';

require.config({

  baseUrl: "./js",

  paths: {
    jquery : "vendor/jquery/jquery",
    backbone : "vendor/backbone/backbone",
    underscore : "vendor/underscore/underscore",
    domReady : "vendor/requirejs-domready/domReady",
    text : "vendor/requirejs-text/text",
    json2 : "vendor/json2/json2",
    handlebars : "vendor/handlebars/handlebars",
    "preview" : "vendor/preview/jquery.preview",
    'backbone.marionette' : "vendor/backbone.marionette/lib/backbone.marionette",
    'marionette_handlebars' : "vendor/backbone.marionette.handlebars/backbone.marionette.handlebars",
    // Application Folders
      // -------------------
    "collections": "app/collections",

    "models": "app/models",

    "controller": "app/controller",

    "routers": "app/routers",

    "templates": "app/templates",

    "views": "app/views"
  },

  shim: {
    jquery: {
      exports: "jQuery"
    },

    preview : {
      deps: ["jquery"]
    },

    underscore: {
      exports: "_"
    },

    handlebars : {
        exports: 'Handlebars'
      },

      backbone: {
        deps: ["underscore" , "jquery"],
        exports: "Backbone"
      },

      "backbone.marionette" : {
        deps: ["backbone"],
        exports: "Backbone.Marionette"
      },

      "marionette_handlebars" : {
        deps: ["backbone.marionette"]
      }

    }

  });


require(["jquery", 'preview', 'backbone','underscore', 'handlebars', 'backbone.marionette', 'marionette_handlebars',
        "app/application",
        "controller/TweetController",
        "routers/AppRouter"
        ], function(
        $,
        preview,
        Backbone,
        _,
        Handlebars,
        Marionette,
        MarionetteHandlebars,
        NewsFeedApp,
        TweetController,
        AppRouter) {

        //initialize the controller and app router
        NewsFeedApp.addInitializer(function(options) {
            // initialize the controller
            var controller = new TweetController({
                region_reader : NewsFeedApp.reader
              });
            // initialize the router
            var router = new AppRouter({
                controller : controller
              });
          });

        //$('#preview').preview({key: window.key});

        //initialize the application
        NewsFeedApp.start({
            root : window.location.pathname,
            path_root : "/"
          });
      }
);