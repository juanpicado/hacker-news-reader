'use strict';

define(["underscore",
  "backbone",
  "backbone.marionette",
  "views/NewsItemView"], function( _, Backbone, Marionette, NewsItemView) {

    //
    var NoItemsView = Backbone.Marionette.ItemView.extend({
        template: {
            type: 'handlebars',
            template: Handlebars.compile($('#show-no-items-message-template').html())
          }
        });

    //
    var newsItemView = Backbone.Marionette.CollectionView.extend({
        //
        id: "newsView",
        //
        itemView: NewsItemView,
        //
        emptyView: NoItemsView,
        //
        onItemAdded: function(itemView) {},
        //
        initialize: function() {
              // grab the child collection from the parent model
              // so that we can render the collection as children
              // of this parent node
              //this.collection = this.model.nodes;
              console.log("tweets view", this.collection);
            }
      });
    return newsItemView;
  }
);
