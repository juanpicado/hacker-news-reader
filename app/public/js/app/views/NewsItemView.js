'use strict';

define(["underscore", "backbone", "backbone.marionette", 'marionette_handlebars'], function( _, Backbone, Marionette, MarionetteHandlebars) {

    var MyItemView = Backbone.Marionette.ItemView.extend({

        //
        className: "news-item",

        //
        loaded : false,

        //
        tagName :'div',

        //
        triggers: {
            "click .comments": "item:load-comments",
            "click .preview": "item:load-preview"
          },

          //
          events: {
            "click .hide-comments": "hideComments",
            "click .hide-preview": "hidePreview"
          },

          //
          template: {
            type: 'handlebars',
            template: Handlebars.compile($('#news-item-view').html())
          },

          //
          hideComments : function() {
            this.$el.find('.hide-comments').hide();
            this.$el.find('.comments').show();
            this.$el.find('.append-detail').hide();
          },

          //
          hidePreview : function() {
              this.$el.find('.hide-preview').hide();
              this.$el.find('.preview').show();
              Backbone.trigger("preview:destroy");
            },

          //
          displayPreview : function() {
              this.$el.find('.hide-preview').show();
              this.$el.find('.preview').hide();
            },

          //
          appendComments : function(commentsView) {
            var comments = this.$el.find('.append-detail');
            comments.append(commentsView.el);
          },

          //
          displayComments : function() {
              this.$el.find('.append-detail').show();
              this.$el.find('.hide-comments').show();
              this.$el.find('.comments').hide();
            },

          //
          onRender: function() {
              this.hideComments();
              this.$el.find('.hide-preview').hide();
            }
        });

    return MyItemView;
  }
);
