'use strict';

define(["underscore", "backbone", "backbone.marionette", 'marionette_handlebars'], function( _, Backbone, Marionette, MarionetteHandlebars) {

  // The recursive tree view
  var TreeView = Backbone.Marionette.CompositeView.extend({
      template: "#node-2-template",

      tagName: "ul",

      initialize: function() {
          // grab the child collection from the parent model
          // so that we can render the collection as children
          // of this parent node
          this.collection = this.model.childrens;
        },

      onBeforeRender : function() {
          console.log("modellll", this.model);
        },

      appendHtml: function(collectionView, itemView) {
          // ensure we nest the child list inside of
          // the current list item
          collectionView.$("li:first").append(itemView.el);
        }
    });

  // The tree's root: a simple collection view that renders
  // a recursive tree structure for each item in the collection
  var TreeRoot = Backbone.Marionette.CollectionView.extend({
      itemView: TreeView
    });

  return TreeRoot;
}
);
