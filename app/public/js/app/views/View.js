'use strict';

// View.js
// -------
define(["jquery", "backbone", "models/NewsItem", "text!templates/heading.html"],

  function($, Backbone, Model, template){

        var View = Backbone.View.extend({
          el : '.example',
          render: function() {
            this.template = _.template(template, {});
            this.$el.html(this.template);
            return this;
          }
        });


        return View;

      }
);