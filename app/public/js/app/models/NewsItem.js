'use strict';

define(["backbone"], function(Backbone) {
	var NewsItem = Backbone.Model.extend();
	return NewsItem;
});
