'use strict';

// DesktopRouter.js
// ----------------
define(["jquery", "backbone", "models/Comment", "views/View", "collections/Collection"],

    function($, Backbone, Model, View, Collection) {

    var DesktopRouter = Backbone.Router.extend({

        initialize: function() {
            // Tells Backbone to start watching for hashchange events
            Backbone.history.start();
          },

        // All of your Backbone Routes (add more)
        routes: {
            // When there is no hash on the url, the home method is called
            "": "index"
          },

          index: function() {
            // Instantiates a new view which will render the header text to the page
            var view = new View();
            console.log("view", view);
            view.render();
            console.log("view1 ", view.$el);
            console.log("view2 ", $('.example'));
            $('.example').append(view.$el);
          }

        });

        // Returns the DesktopRouter className
    return DesktopRouter;

  }
);