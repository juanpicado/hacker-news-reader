'use strict';

define(["backbone", "backbone.marionette"], function(Backbone, Marionette) {
  var Router = Marionette.AppRouter.extend({
    appRoutes: {
      "": "listNews"
    }
  });

  return Router;
});