'use strict';

define(['backbone', 'models/NewsItem'], function(Backbone, NewsItem) {
    var Tweets = Backbone.Collection.extend({
        model: NewsItem,
        initialize : function () {
            this.bind("reset", function (model, options) {

            });
          },
        // Url to request when fetch() is called
        // Because twitter doesn't return an array of models by default we need
        // to point Backbone.js at the correct property
        parse: function(response) {
            return response.items.slice(0, 10);
          },

        url: function () {
            return 'http://hndroidapi.appspot.com/newest/format/json/page/?appid=HackerNewsDroid';
          },

        // Overwrite the sync method to pass over the Same Origin Policy
        sync: function(method, model, options) {
            var that = this;
            var params = _.extend({
                type: 'GET',
                dataType: 'jsonp',
                contentType: 'application/json',
                async: false,
                url: that.url(),
                processData: false
              }, options);
            return $.ajax(params);
          }
      });
    return Tweets;
  }
);
