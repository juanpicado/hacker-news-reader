'use strict';

define(['backbone', 'models/Comment'], function(Backbone, Comment) {
    var Tweets = Backbone.Collection.extend({
        model: Comment,
        initialize : function () {
            this.bind("reset", function (model, options) {

            });
          },
        // Url to request when fetch() is called
        // url: 'http://hndroidapi.appspot.com/nestedcomments/format/json/id/',
        // Because twitter doesn't return an array of models by default we need
        // to point Backbone.js at the correct property
        parse: function(response) {
            console.log("items", response.items);
            return response.items.slice(0, 20);
          },
        url: function () {
            return 'http://hndroidapi.appspot.com/nestedcomments/format/json/id/' + this.itemId + '?appid=HackerNewsDroid';
            //return 'http://hndroidapi.appspot.com/nestedcomments/format/json/id/5618409?appid=HackerNewsDroid';
          },

        itemId: 1,

        // Overwrite the sync method to pass over the Same Origin Policy
        sync: function(method, model, options) {
            var that = this;
            var params = _.extend({
                type: 'GET',
                dataType: 'jsonp',
                contentType: 'application/json',
                async: false,
                url: that.url(),
                processData: false
              }, options);
            return $.ajax(params);
          }
      });
    return Tweets;
  }
);
