'use strict';

define(["underscore", "backbone", 'collections/NewsCollection','backbone.marionette'],

function( _, Backbone, NewsCollection) {

	var NewsApp = new Backbone.Marionette.Application();

	NewsApp.addRegions({
		reader: '#reader'
	});

	NewsApp.on('initialize:after', function(options) {
		Backbone.history.start();
	});

	NewsApp.on("initialize:before", function(options) {
		//TODO: something before star the app
	});

	return NewsApp;
});
