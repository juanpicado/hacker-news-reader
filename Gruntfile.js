/*global require:false, module:false*/

'use strict';

var path = require('path');

var lrSnippet = require('grunt-contrib-livereload/lib/utils').livereloadSnippet;

var folderMount = function folderMount(connect, point) {
  return connect['static'](path.resolve(point));
};

module.exports = function(grunt) {

  grunt.initConfig({

    pkg: grunt.file.readJSON('package.json'),

    //

    dirs: {
      root: 'app/public',
      staging: 'temp/staging',
      dist: 'dist',
      sass: 'app/sass',
      test: 'app/test'
    },

    // requirejs definition

    requirejs: {
      javascript: {
        options: {
          baseUrl: "<%= dirs.staging %>/step1/js/",
          paths: {
            "desktop": "app/main"
          },
          wrap: true,
          name: "libs/almond",
          preserveLicenseComments: false,
          optimize: "none",
          mainConfigFile: "<%= dirs.staging %>/step1/js/app/main.js",
          include: ["desktop"],
          out: "<%= dirs.staging %>/step1/js/app/init/require.min.js"
        }
      },
      css: {
        options: {
          optimizeCss: "standard",
          cssIn: "<%= dirs.staging %>/step1/css/app.css",
          out: "<%= dirs.staging %>/step1/css/app.min.css"
        }
      }
    },

    // compass configuration

    compass: {
      options: {
        sassDir: '<%= dirs.sass %>',
        raw:  'images_dir = "app/public/img"\n' +
              'http_images_path = "../img"\n' +
              'http_javascripts_path = "../js"\n' +
              'http_stylesheets_path = "."\n'
      },
      dev: {
        options: {
          cssDir: '<%= dirs.root %>/css',
          environment: 'development'
        }
      },
      dist: {
        options: {
          cssDir: '<%= dirs.staging %>/step1/css',
          environment: 'production',
          force: true
        }
      }
    },

    clean: {
      dist: ['<%= dirs.staging %>','<%= dirs.dist %>']
    },

    // jslint configuration

    jshint: {
      options: {
        jshintrc: './.jshintrc'
      },
      gruntfile: ['Gruntfile.js'],
      js: ['<%= dirs.root %>/js/app/**/*.js'],
      'test-unit': ['<%= dirs.test %>/unit/**/*.js'],
      dev: {
        /*options: {
          warnOnly: true
        },*/
        src: [ '<%= jshint.gruntfile %>',
               '<%= jshint.js %>' ,
               "<%= jshint['test-unit'] %>" ]
      },
      build: {
        src: [ '<%= jshint.gruntfile %>',
               '<%= jshint.js %>' ]
      }
    },

    // server configuration

    server: {
        dev: {
          options: {
            port: 9001,
            base: '<%= dirs.root %>',
            keepalive: false,
            middleware: function(connect, options) {
              return [
                lrSnippet,
                folderMount(connect, options.base)
              ];
            }
          }
        },
        build: {
          options: {
            port: 9003,
            base: '<%= dirs.staging %>/step3',
            keepalive: false
          }
        },
        dist: {
          options: {
            port: 9002,
            base: '<%= dirs.dist %>/public',
            keepalive: true
          }
        }
      },

      imagemin: {
        dist: {
          expand: true,
          cwd: '<%= dirs.staging %>/step1',
          src: 'img/**',
          dest: '<%= dirs.staging %>/step2/'
        }
      },

      htmlmin: {
        dist: {
          options: {
            removeComments: true,
            collapseWhitespace: true
          },
          expand: true,
          cwd: '<%= dirs.staging %>/step2',
          src: '**/*.html',
          dest: '<%= dirs.staging %>/step3/'
        }
      },

      rev: {
        files: {
          expand: true,
          cwd: '<%= dirs.staging %>/step2',
          src: [  'js/**/*.js', '!js/lib/*.js',
                  'css/**/*.css',
                  'img/**/*.{png,jpg}']
        }
      },

      copy: {
        'dist-step-1': {
          expand: true,
          cwd: '<%= dirs.root %>',
          src: ['**'],
          dest: '<%= dirs.staging %>/step1/'
        },
        'dist-step-2': {
          expand: true,
          cwd: '<%= dirs.staging %>/step1',
          src: [
            "*",
            "partials/**",
            "**/*.min.*",
            "ico/**"
          ],
          dest:  "<%= dirs.staging %>/step2/"
        },
        'dist-step-3': {
          expand: true,
          cwd: '<%= dirs.staging %>/step2',
          src: [
              "**",
              "!**/*.html",
              "partials/**"
            ],
            dest:  "<%= dirs.staging %>/step3/"
          },
          'dist-final': {
            expand: true,
            cwd: '<%= dirs.staging %>/step3',
            src: ["**"],
            dest:  "<%= dirs.dist %>/"
          }
        },

        //

        'useminPrepare': {
          options: {
            dest: '<%= dirs.staging %>/step2'
          },
          html: '<%= dirs.staging %>/step1/*.html'
        },

        usemin: {
          options: {
            basedir: '<%= dirs.staging %>/step2'
          },
          html: {
            expand:true,
            cwd: '<%= dirs.staging %>/step2',
            src:['**/*.{html,css}']
          }
        },

        manifest:{
          dest: '<%= dirs.staging %>/step3/manifest.appcache',
          port: 3002
        },

        // regarde configuration

        regarde: {
          html: {
            files: ['<%= dirs.root %>/index.html'],
            tasks: ['livereload'],
            spawn: false
          },
          js: {
            files: ['<%= dirs.root %>/js/**/*.js'],
            tasks: ['jshint:dev', 'livereload', 'wait:100'],
            spawn: false
          },
          compass: {
            files: [ '<%= dirs.sass %>/*.sass', '<%= dirs.sass %>/*.scss' ],
            tasks: [ 'compass:dev', 'livereload'],
            spawn: false
          },
          'test-unit': {
            files: ['<%= dirs.test %>/unit/**/*.js'],
            tasks: ['jshint:dev', 'wait:100' , 'testacularRun:auto'],
            spawn: false
          },
          gruntfile: {
            files: ['Gruntfile.js'],
            tasks: ['wait:100', 'jshint:dev'],
            spawn: false
          }
        }
      });


  // required modules

  grunt.loadNpmTasks('grunt-contrib-connect');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-requirejs');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-regarde');
  grunt.loadNpmTasks('grunt-contrib-livereload');
  grunt.loadNpmTasks('grunt-contrib-compass');
  grunt.loadNpmTasks('grunt-usemin');
  grunt.loadNpmTasks('grunt-rev');
  grunt.loadNpmTasks('grunt-contrib-imagemin');
  grunt.loadNpmTasks('grunt-contrib-htmlmin');
  grunt.loadNpmTasks('grunt-contrib-manifest');

  // test modules definition

  //grunt.loadNpmTasks('grunt-jasmine-runner');

  // task definition

  grunt.registerTask('test', ['jshint']);

  grunt.registerTask('build', ['clean:dist',
    'copy:dist-step-1',
    'compass:dist',
    'useminPrepare',
    'requirejs:javascript',
    'requirejs:css',
    'copy:dist-step-2',
    'imagemin',
    'rev',
    'usemin',
    'copy:dist-step-3',
    'htmlmin:dist',
    //'server:build',
    'manifest',
    'copy:dist-final',
    'time'
    ]);

  grunt.renameTask('connect', 'server');

  //
  grunt.registerTask('wait', 'Wait for a set amount of time(ms).', function(delay) {
    if (delay) {
      var done = this.async();
      setTimeout(done, delay );
    }
  });

  var now = +new Date();
  grunt.registerTask('time', 'Print sucess status with elapsed time', function() {
    grunt.log.ok('Build success. Done in ' + ((+new Date() - now) / 1000) + 's');
  });

  grunt.registerTask('dev', ['jshint:dev', 'compass:dev', 'livereload-start' , 'server:dev', 'regarde']);

   // Default task.
  grunt.registerTask('live', 'dev');

  grunt.registerTask('default', ['test', 'build']);
};